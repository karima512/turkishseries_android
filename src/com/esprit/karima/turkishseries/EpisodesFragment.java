package com.esprit.karima.turkishseries;

import java.util.ArrayList;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.esprit.karima.turkishseries.entities.Serie;
import com.esprit.karima.turkishseries.utils.GlobalClass;
import com.esprit.karima.turkishseries.utils.Item;

public class EpisodesFragment extends Fragment implements Item{
	private ListView list;
	private CustomListviewAdapter adapter;
	private ArrayList<Serie> series;
	private GlobalClass global;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = (GlobalClass) getActivity().getBaseContext()
				.getApplicationContext();
		series = global.seriesTemp;

		View rootView = inflater.inflate(R.layout.activity_main, container,
				false);

		rootView.setFocusableInTouchMode(true);
		rootView.requestFocus();
		rootView.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				Log.i("test", "keyCode: " + keyCode);
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					getActivity().getSupportFragmentManager().popBackStack(
							null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					return true;
				} else {
					return false;
				}
			}
		});
		Resources res = getResources();
		list = (ListView) rootView.findViewById(R.id.series_listview);
		adapter = new CustomListviewAdapter(getActivity(), series, res, false);
		list.setAdapter(adapter);
		return rootView;
	}

	@Override
	public boolean isSection() {
		// TODO Auto-generated method stub
		return false;
	}
}
