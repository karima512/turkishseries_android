package com.esprit.karima.turkishseries;

import java.util.ArrayList;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.esprit.karima.turkishseries.entities.FavorisItem;
import com.esprit.karima.turkishseries.entities.Serie;
import com.esprit.karima.turkishseries.utils.GlobalClass;
import com.esprit.karima.turkishseries.utils.Item;
import com.esprit.karima.turkishseries.utils.SectionItem;

public class SearchFragment extends Fragment implements Item {
	private ListView list;
	private EditText search;
	private SearchCustomListviewAdapter adapter;
	ArrayList<Item> items = new ArrayList<Item>();
	private GlobalClass global;
	private TextWatcher watcher;
	private Resources res;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = (GlobalClass) getActivity().getBaseContext()
				.getApplicationContext();

		View rootView = inflater.inflate(R.layout.activity_search, container,
				false);
		watcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Load();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		};
		search = (EditText) rootView.findViewById(R.id.query);
		
		search.setHint(global.language.equals("1")? "اكتب كلمة المفتاح هنا": "Burada anahtar sözcükler yazın");

		search.addTextChangedListener(watcher);
		rootView.setFocusableInTouchMode(true);
		rootView.requestFocus();
		rootView.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					getActivity().getSupportFragmentManager().popBackStack(
							null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					return true;
				} else {
					return false;
				}
			}
		});
		res = getResources();
		list = (ListView) rootView.findViewById(R.id.series_listview);
		ArrayList<Item> data = new ArrayList<Item>();
		ArrayList<Serie> listeSeries = new ArrayList<Serie>();
		ArrayList<Serie> listeEpisodes = new ArrayList<Serie>();
		for (Serie serie : global.allSeriesList) {
			listeSeries.add(serie);
		}
		for (Serie serie : global.allEpisodesList) {
			listeEpisodes.add(serie);
		}
		data.add(new SectionItem("Series"));
		for (Serie ser : listeSeries) {
			data.add(new FavorisItem(ser, true));
		}
		data.add(new SectionItem("Episodes"));
		for (Serie ser : listeEpisodes) {
			data.add(new FavorisItem(ser, false));
		}
		adapter = new SearchCustomListviewAdapter(getActivity(), data, res,
				false);
		list.setAdapter(adapter);
		return rootView;
	}

	private void Load() {
		ArrayList<Item> data = new ArrayList<Item>();
		ArrayList<Serie> listeSeries = new ArrayList<Serie>();
		ArrayList<Serie> listeEpisodes = new ArrayList<Serie>();
		for (Serie serie : global.allSeriesList) {
			if (serie.getName().contains(search.getText().toString()))
				listeSeries.add(serie);
		}
		for (Serie serie : global.allEpisodesList) {
			if (serie.getName().contains(search.getText().toString()))
				listeEpisodes.add(serie);
		}
		data.add(new SectionItem("Series"));
		for (Serie ser : listeSeries) {
			data.add(new FavorisItem(ser, true));
		}
		data.add(new SectionItem("Episodes"));
		for (Serie ser : listeEpisodes) {
			data.add(new FavorisItem(ser, false));
		}
		adapter = new SearchCustomListviewAdapter(getActivity(), data, res,
				false);
		list.setAdapter(adapter);
	}

	@Override
	public boolean isSection() {
		return false;
	}
}
