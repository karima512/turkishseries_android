package com.esprit.karima.turkishseries;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.esprit.karima.turkishseries.entities.FavorisItem;
import com.esprit.karima.turkishseries.utils.GlobalClass;
import com.esprit.karima.turkishseries.utils.ImageDownloader;
import com.esprit.karima.turkishseries.utils.Item;
import com.esprit.karima.turkishseries.utils.LocalPath;
import com.esprit.karima.turkishseries.utils.SectionItem;

public class FavoriteCustomListviewAdapter extends BaseAdapter {
	private FragmentActivity activity;
	private ArrayList<?> data;
	private static LayoutInflater inflater = null;
	public Resources res;
	private GlobalClass global;
	FavorisItem tempValues = null;
	int i = 0;

	public FavoriteCustomListviewAdapter(FragmentActivity a, ArrayList<?> d,
			Resources resLocal, boolean isSeriesList) {
		global = (GlobalClass) a.getBaseContext().getApplicationContext();
		activity = a;
		data = d;
		res = resLocal;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		if (data.size() <= 0)
			return 1;
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		public TextView text;
		public TextView text1;
		public TextView textWide;
		public ImageView image;
		public ImageButton stars;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder holder;

		final Item i = (Item) data.get(position);
		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;
				vi = inflater.inflate(R.layout.list_item_section, null);

				vi.setOnClickListener(null);
				vi.setOnLongClickListener(null);
				vi.setLongClickable(false);

				final TextView sectionView = (TextView) vi
						.findViewById(R.id.list_item_section_text);
				sectionView.setText(si.getTitle());
			} else {
				tempValues = null;
				tempValues = (FavorisItem) data.get(position);
				vi = inflater
						.inflate(
								R.layout.item_listview_serie,
								null);
				holder = new ViewHolder();
				holder.text = (TextView) vi.findViewById(R.id.serie_title);
				holder.stars = (ImageButton) vi.findViewById(R.id.serie_rate);
				holder.image = (ImageView) vi.findViewById(R.id.serie_icon);
				vi.setTag(holder);
				if (data.size() <= 0) {
					holder.text.setText("No Data");
				} else {
					holder = new ViewHolder();
					holder.text = (TextView) vi.findViewById(R.id.serie_title);
					holder.stars = (ImageButton) vi
							.findViewById(R.id.serie_rate);
					holder.image = (ImageView) vi.findViewById(R.id.serie_icon);
					vi.setTag(holder);

					holder.text.setText(tempValues.getName());
					new ImageDownloader(holder.image)
							.execute(LocalPath.localPath + "/files/"
									+ tempValues.getImage());
					vi.setOnClickListener(new OnItemClickListener(position));
					holder.stars.setTag(Boolean.valueOf(true));
					holder.stars
							.setImageResource(R.drawable.star_big_on);
				}
			}
		}
		return vi;
	}

	private class OnItemClickListener implements OnClickListener {
		private int mPosition;

		OnItemClickListener(int position) {
			mPosition = position;
		}

		@Override
		public void onClick(View arg0) {
			FavorisItem item = (FavorisItem) data.get(mPosition);
			if (item.isSerie()) {
				global.seriesTemp = global.getEpisodesBySerieId(item.getID());
				Fragment fragment = new EpisodesFragment();
				FragmentManager fragmentManager = activity
						.getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.addToBackStack(null).commit();
			} else {
				Log.i("link", ((FavorisItem) data.get(mPosition)).getLink());
				Intent i = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse(((FavorisItem) data.get(mPosition)).getLink()));
				activity.startActivity(i);
			}
		}
	}
}