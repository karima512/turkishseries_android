package com.esprit.karima.turkishseries;

import com.esprit.karima.turkishseries.utils.GlobalClass;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

public class AboutUsFragment extends Fragment{
	
	private GlobalClass global;
	private View about;
	private ImageButton email;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = (GlobalClass) getActivity().getBaseContext().getApplicationContext();
		
		View rootView = inflater.inflate(R.layout.about_us, container,
				false);
//		Resources res = getResources();
//		about = (View) rootView.findViewById(R.layout.about_us);
		email = (ImageButton) rootView.findViewById(R.id.emailButton);
		email.setOnClickListener(new View.OnClickListener() {

		      @Override
		      public void onClick(View view) {
		        Intent intent = new Intent(getActivity(), SendEmail.class);
		        startActivity(intent);
		      }

		    });
		
		
		
		return rootView;
	}


}
