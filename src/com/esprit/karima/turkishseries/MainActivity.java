package com.esprit.karima.turkishseries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.esprit.karima.turkishseries.utils.ConnectionDetector;
import com.esprit.karima.turkishseries.utils.GlobalClass;

public class MainActivity extends ActionBarActivity implements
		OnItemClickListener, OnClickListener {
	private static String[] arabicTitles = { "مسلسلات تركية",
			"السلسلة المفضلة","بحث", "حول التطبيق", "إعدادات" };
	private static String[] turkishTitles = { "Türk Serisi", "Seri favori","Arama",
			"Hakkımızda", "Ayarlar" };
	private GlobalClass global;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView mDrawerList;

	// flag for Internet connection status
    Boolean isInternetPresent = false;     
    // Connection detector class
    ConnectionDetector cd;

	
	// Dialog
	private Button dialogConfirm;
	private Dialog dialog;
	private ListView listeLangues;
	private TextView labelTitre;
	private String oldLanguage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		global = (GlobalClass) getBaseContext().getApplicationContext();
		global.Init();
		oldLanguage = global.getLanguageFromPhone();
		if (global.firstTime) {
			showDialog();
		}
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, global.getLanguageFromPhone()
						.equals("1") ? arabicTitles : turkishTitles));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				supportInvalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				supportInvalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}
	}
	/*************** showAlertDialog Check Internet Connection ******************/
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
 
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	 Intent callConnectionSettingIntent = new Intent(
                         android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                 MainActivity.this.startActivity(callConnectionSettingIntent);
            }
        });
        alertDialog.setButton2("Cancel" , new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
             //   finish();
            }
        });
 
        // Showing Alert Message
        alertDialog.show();
    }
	/*************** showAlertDialog Choice Langue ******************/
	public void showDialog() {
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_lang_select);
		dialog.setCanceledOnTouchOutside(true);
		listeLangues = (ListView) dialog.findViewById(R.id.dialog_listview);
		dialogConfirm = (Button) dialog.findViewById(R.id.dialog_select);
		labelTitre = (TextView) dialog.findViewById(R.id.dialog_choose_lang);
		dialogConfirm.setOnClickListener(this);
		String[] langues = new String[] { "العربية", "Türk" };
		int[] flags = new int[] { R.drawable.arabic, R.drawable.turkish };
		List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i < 2; i++) {
			HashMap<String, String> hm = new HashMap<String, String>();
			hm.put("txt", langues[i]);
			hm.put("flag", Integer.toString(flags[i]));
			aList.add(hm);
		}
		String[] from = { "flag", "txt" };
		int[] to = { R.id.flag, R.id.txt };
		SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList,
				R.layout.item_listview_langue, from, to);
		listeLangues.setAdapter(adapter);
		listeLangues.setOnItemClickListener(this);
		dialog.show();
		dialog.setCancelable(false);
		listeLangues.requestFocusFromTouch();
		int lang = Integer.parseInt(global.language) - 1;
		listeLangues.setSelection(lang);
		listeLangues
				.performItemClick(
						listeLangues.getAdapter().getView(lang, null, null),
						lang, lang);
	}

	@Override
	public void onClick(View v) {
		dialog.dismiss();
		global.SaveFirstTime();
		if (!oldLanguage.equals(global.getLanguageFromPhone())) {
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (position == 0) {
			setTitle("مسلسلات تركية");
			labelTitre.setText("إختر اللغة");
			dialogConfirm.setText("إختيار");
			global.setLanguageFromPhone("1");
		} else if (position == 1) {
			setTitle("türk dizileri");
			labelTitre.setText("Dili seçin");
			dialogConfirm.setText("Seçmek");
			global.setLanguageFromPhone("2");
		}
	}

	private void selectItem(int position) {
		switch (position) {
		case 0:
			Fragment fragment = new SeriesFragment();
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment).commit();

			mDrawerList.setItemChecked(position, true);
			setTitle(global.getLanguageFromPhone().equals("1") ? arabicTitles[position]
					: turkishTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 1:
			Fragment fragmentFavorite = new FavoriteFragment();
			FragmentManager fragmentManager2 = getSupportFragmentManager();
			fragmentManager2.beginTransaction()
			.replace(R.id.content_frame, fragmentFavorite)
			.addToBackStack(null).commit();

			mDrawerList.setItemChecked(position, true);
			setTitle(global.getLanguageFromPhone().equals("1") ? arabicTitles[position]
					: turkishTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 3:
			Fragment fragmentAbout = new AboutUsFragment();
			FragmentManager fragmentManager3 = getSupportFragmentManager();
			fragmentManager3.beginTransaction()
			.replace(R.id.content_frame, fragmentAbout)
			.addToBackStack(null).commit();

			mDrawerList.setItemChecked(position, true);
			setTitle(global.getLanguageFromPhone().equals("1") ? arabicTitles[position]
					: turkishTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 2:
			Fragment fragmentSearch = new SearchFragment();
			FragmentManager fragmentManager4 = getSupportFragmentManager();
			fragmentManager4.beginTransaction()
			.replace(R.id.content_frame, fragmentSearch)
			.addToBackStack(null).commit();

			mDrawerList.setItemChecked(position, true);
			setTitle(global.getLanguageFromPhone().equals("1") ? arabicTitles[position]
					: turkishTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
			
		case 4:
			showDialog();
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		default:
			Toast.makeText(this, "Not implemented yet !", Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}
	
	@Override
	  public void onResume() {
		// creating connection detector class instance
      cd = new ConnectionDetector(getApplicationContext());
      isInternetPresent = cd.isConnectingToInternet();
      // check for Internet status
      if (isInternetPresent) {
          // Internet Connection is Present
          // make HTTP requests
      	Log.e("Internet Connection", "You have internet connection");
      } else {
          // Internet connection is not present
          // Ask user to connect to Internet
          showAlertDialog(MainActivity.this, "No Internet Connection",
                  "You don't have internet connection.", false);
      }
      super.onResume();
	  }
}
