package com.esprit.karima.turkishseries;

import java.util.ArrayList;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.esprit.karima.turkishseries.entities.Serie;
import com.esprit.karima.turkishseries.utils.GlobalClass;
import com.esprit.karima.turkishseries.utils.Item;


public class SeriesFragment extends Fragment implements Item{
	private ListView list;
	private CustomListviewAdapter adapter;
	private ArrayList<Serie> series;
	private GlobalClass global;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = (GlobalClass) getActivity().getBaseContext().getApplicationContext();
		series = global.getSeriesByLang();
		View rootView = inflater.inflate(R.layout.activity_main, container,
				false);
		Resources res = getResources();
		list = (ListView) rootView.findViewById(R.id.series_listview);
		adapter = new CustomListviewAdapter(getActivity(), series, res, true);
		list.setAdapter(adapter);
		return rootView;
	}

	@Override
	public boolean isSection() {
		// TODO Auto-generated method stub
		return false;
	}
}
