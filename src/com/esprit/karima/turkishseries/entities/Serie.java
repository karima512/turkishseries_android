package com.esprit.karima.turkishseries.entities;

public class Serie {
	private String ID;
	private String langueseries;
	private String name;
	private String image;
	private String link;

	public Serie(String iD, String langueseries, String name, String image) {
		super();
		ID = iD;
		this.langueseries = langueseries;
		this.name = name;
		this.image = image;
	}

	public Serie(String iD, String langueseries, String name, String image,
			String link) {
		super();
		ID = iD;
		this.langueseries = langueseries;
		this.name = name;
		this.image = image;
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "Serie [ID=" + ID + ", langueseries=" + langueseries + ", name="
				+ name + ", image=" + image + ", link=" + link + "]";
	}

	public Serie() {
		super();
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getLangueseries() {
		return langueseries;
	}

	public void setLangueseries(String langueseries) {
		this.langueseries = langueseries;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result
				+ ((langueseries == null) ? 0 : langueseries.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Serie other = (Serie) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (langueseries == null) {
			if (other.langueseries != null)
				return false;
		} else if (!langueseries.equals(other.langueseries))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
