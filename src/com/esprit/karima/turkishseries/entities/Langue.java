package com.esprit.karima.turkishseries.entities;

public class Langue {
	private String ID;
	private String langueseries;
	private String orientation;
	private String image;

	public Langue() {
		super();
	}

	public Langue(String iD, String langueseries, String orientation,
			String image) {
		super();
		ID = iD;
		this.langueseries = langueseries;
		this.orientation = orientation;
		this.image = image;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getLangueseries() {
		return langueseries;
	}

	public void setLangueseries(String langueseries) {
		this.langueseries = langueseries;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Langue [ID=" + ID + ", langueseries=" + langueseries
				+ ", orientation=" + orientation + ", image=" + image + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result
				+ ((langueseries == null) ? 0 : langueseries.hashCode());
		result = prime * result
				+ ((orientation == null) ? 0 : orientation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Langue other = (Langue) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (langueseries == null) {
			if (other.langueseries != null)
				return false;
		} else if (!langueseries.equals(other.langueseries))
			return false;
		if (orientation == null) {
			if (other.orientation != null)
				return false;
		} else if (!orientation.equals(other.orientation))
			return false;
		return true;
	}

}
