package com.esprit.karima.turkishseries.entities;

import java.io.Serializable;


public class Favoris implements Serializable{

	private static final long serialVersionUID = 1L;
	private boolean isSerie;
	private String ID;
	private String link;
	private String image;
	private String name;
	public boolean isSerie() {
		return isSerie;
	}
	public void setSerie(boolean isSerie) {
		this.isSerie = isSerie;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "Favoris [isSerie=" + isSerie + ", ID=" + ID + ", link=" + link
				+ ", image=" + image + ", name=" + name + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + (isSerie ? 1231 : 1237);
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favoris other = (Favoris) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (isSerie != other.isSerie)
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public Favoris(boolean isSerie, String iD, String link, String image,
			String name) {
		super();
		this.isSerie = isSerie;
		ID = iD;
		this.link = link;
		this.image = image;
		this.name = name;
	}
	public Favoris() {
		super();
	}
	public Favoris(String iD, String link, String image, String name) {
		super();
		isSerie = false;
		ID = iD;
		this.link = link;
		this.image = image;
		this.name = name;
	}

	
}
