package com.esprit.karima.turkishseries.dao;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.esprit.karima.turkishseries.entities.Serie;
import com.esprit.karima.turkishseries.utils.LocalPath;
import com.esprit.karima.turkishseries.utils.RequestUrl;

public class SeriesDao {
	@SuppressWarnings("unchecked")
	public ArrayList<Serie> getSeries() {
		String response = "";

		ArrayList<NameValuePair> url = new ArrayList<NameValuePair>();
		url.add(new BasicNameValuePair("url", LocalPath.localPath
				+ "scripts/getListSeries.php"));
		ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
		args.add(new BasicNameValuePair("Token", "turkish2014"));
		args.add(new BasicNameValuePair("Langue", "1"));
		try {
			response = new RequestUrl().execute(url, args).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (response.equals("\"Invalid arguments.\"") || response.equals("[]")) {
			return null;
		} else {
			ArrayList<Serie> series = new ArrayList<Serie>();
			try {
				JSONArray array = new JSONArray(response);
				if (array.length() > 0) {
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						Serie serie = new Serie();
						serie.setID(obj.getString("ID"));
						serie.setLangueseries(obj.getString("langueseries"));
						serie.setName(new String(obj.getString("name")
								.getBytes(), "utf-8"));
						serie.setImage(obj.getString("image"));
						series.add(serie);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return series;
		}
	}
	

	@SuppressWarnings("unchecked")
	public ArrayList<Serie> getAllSeries() {
		String response = "";

		ArrayList<NameValuePair> url = new ArrayList<NameValuePair>();
		url.add(new BasicNameValuePair("url", LocalPath.localPath
				+ "scripts/getAllSeries.php"));
		ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
		args.add(new BasicNameValuePair("Token", "turkish2014"));
		try {
			response = new RequestUrl().execute(url, args).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (response.equals("\"Invalid arguments.\"") || response.equals("[]")) {
			return null;
		} else {
			ArrayList<Serie> series = new ArrayList<Serie>();
			try {
				JSONArray array = new JSONArray(response);
				if (array.length() > 0) {
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						Serie serie = new Serie();
						serie.setID(obj.getString("ID"));
						serie.setLangueseries(obj.getString("langueseries"));
						serie.setName(new String(obj.getString("name")
								.getBytes(), "utf-8"));
						serie.setImage(obj.getString("image"));
						series.add(serie);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return series;
		}
	}
}
