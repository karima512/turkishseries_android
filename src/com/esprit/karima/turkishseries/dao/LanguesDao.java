package com.esprit.karima.turkishseries.dao;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.esprit.karima.turkishseries.entities.Langue;
import com.esprit.karima.turkishseries.utils.LocalPath;
import com.esprit.karima.turkishseries.utils.RequestUrl;

public class LanguesDao {
	@SuppressWarnings("unchecked")
	public ArrayList<Langue> getLangues() {
		String response = "";
		ArrayList<NameValuePair> url = new ArrayList<NameValuePair>();
		url.add(new BasicNameValuePair("url", LocalPath.localPath
				+ "scripts/getLangue.php"));
		ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
		args.add(new BasicNameValuePair("Token", "turkish2014"));
		try {
			response = new RequestUrl().execute(url, args).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (response.equals("\"Invalid arguments.\"") || response.equals("[]")) {
			return null;
		} else {
			ArrayList<Langue> langues = new ArrayList<Langue>();
			try {
				JSONArray array = new JSONArray(response);
				if (array.length() > 0) {
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						Langue langue = new Langue();
						langue.setID(obj.getString("ID"));
						langue.setLangueseries(obj.getString("langueseries"));
						langue.setOrientation(obj.getString("orientation"));
						langue.setImage(obj.getString("image"));
						langues.add(langue);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return langues;
		}
	}
}
