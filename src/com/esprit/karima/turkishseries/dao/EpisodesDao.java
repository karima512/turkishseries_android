package com.esprit.karima.turkishseries.dao;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.esprit.karima.turkishseries.entities.Serie;
import com.esprit.karima.turkishseries.utils.LocalPath;
import com.esprit.karima.turkishseries.utils.RequestUrl;

public class EpisodesDao {
	@SuppressWarnings("unchecked")
	public ArrayList<Serie> getEpisodes(Serie serie) {
		String response = "";

		ArrayList<NameValuePair> url = new ArrayList<NameValuePair>();
		url.add(new BasicNameValuePair("url", LocalPath.localPath
				+ "scripts/getListEpisodes.php"));
		ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
		args.add(new BasicNameValuePair("Token", "turkish2014"));
		args.add(new BasicNameValuePair("Serie", serie.getID()));
		try {
			response = new RequestUrl().execute(url, args).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (response.equals("\"Invalid arguments.\"") || response.equals("[]")) {
			return null;
		} else {
			ArrayList<Serie> episodes = new ArrayList<Serie>();
			try {
				JSONArray array = new JSONArray(response);
				if (array.length() > 0) {
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						Serie episode = new Serie();
						episode.setID(obj.getString("ID"));
						episode.setLangueseries(obj.getString("name"));
						episode.setName(new String(obj.getString("nameepisode")
								.getBytes(), "utf-8"));
						episode.setImage(serie.getImage());
						episode.setLink(obj.getString("urlepisode"));
						episodes.add(episode);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return episodes;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Serie> getAllEpisodes() {
		String response = "";

		ArrayList<NameValuePair> url = new ArrayList<NameValuePair>();
		url.add(new BasicNameValuePair("url", LocalPath.localPath
				+ "scripts/getAllEpisodes.php"));
		ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
		args.add(new BasicNameValuePair("Token", "turkish2014"));
		try {
			response = new RequestUrl().execute(url, args).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (response.equals("\"Invalid arguments.\"") || response.equals("[]")) {
			return null;
		} else {
			ArrayList<Serie> episodes = new ArrayList<Serie>();
			try {
				JSONArray array = new JSONArray(response);
				if (array.length() > 0) {
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						Serie episode = new Serie();
						episode.setID(obj.getString("ID"));
						episode.setLangueseries(obj.getString("name"));
						episode.setName(new String(obj.getString("nameepisode")
								.getBytes(), "utf-8"));
						episode.setLink(obj.getString("urlepisode"));
						episodes.add(episode);
					}
				}
			}catch (JSONException e) {
				e.printStackTrace();
			}catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return episodes;
		}
	}
}
