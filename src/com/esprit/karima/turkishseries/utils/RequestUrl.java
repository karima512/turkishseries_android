package com.esprit.karima.turkishseries.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

public class RequestUrl extends
		AsyncTask<ArrayList<NameValuePair>, String, String> {

	@Override
	protected String doInBackground(
			@SuppressWarnings("unchecked") ArrayList<NameValuePair>... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response;
		HttpPost httpPost;
		String responseString = null;
		httpPost = new HttpPost(params[0].get(0).getValue());
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params[1]));
			response = httpClient.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
			} else {
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseString;
	}

}
