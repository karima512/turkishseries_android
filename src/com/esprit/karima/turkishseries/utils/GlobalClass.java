package com.esprit.karima.turkishseries.utils;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.esprit.karima.turkishseries.dao.EpisodesDao;
import com.esprit.karima.turkishseries.dao.SeriesDao;
import com.esprit.karima.turkishseries.entities.Favoris;
import com.esprit.karima.turkishseries.entities.Serie;

public class GlobalClass extends Application {
	public boolean firstTime = true;
	public ArrayList<Serie> seriesTemp;
	public String language;
	public ArrayList<Serie> allSeriesList;
	public ArrayList<Serie> allEpisodesList;
	public ArrayList<Favoris> allFavoris;

	// Cette m�thode sera appel�e au lancement de l'application
	public void Init() {
		// Voir si c'est la premi�re ex�cution de l'utilisateur
		SharedPreferences params = getSharedPreferences(
				"com.esprit.karima.turkish", Context.MODE_PRIVATE);
		firstTime = params.getBoolean("firstTime", true);

		// Lecture de toutes les �pisodes et les s�ries
		allSeriesList = new SeriesDao().getAllSeries();
		allEpisodesList = new EpisodesDao().getAllEpisodes();

		// Remplissage des images des �pisodes
		if (allSeriesList.size() > 0 || allSeriesList != null) {
			for (Serie s : allSeriesList) {
				for (int i = 0; i < allEpisodesList.size(); i++) {
					if (s.getID().equals(
							allEpisodesList.get(i).getLangueseries())) {
						allEpisodesList.get(i).setImage(s.getImage());
					}
				}
			}
		}
		// Charge les favoris du t�l�phone
		GetFavoris();
	}

	// Retourne la liste des favoris
	@SuppressWarnings("unchecked")
	public ArrayList<Favoris> GetFavoris() {
		SharedPreferences params = getSharedPreferences(
				"com.esprit.karima.turkish", Context.MODE_PRIVATE);
		try {
			allFavoris = (ArrayList<Favoris>) ObjectSerializer
					.deserialize(params.getString("Favoris", ObjectSerializer
							.serialize(new ArrayList<Favoris>())));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return allFavoris;
	}

	public void SaveFavoris() {
		SharedPreferences params = getSharedPreferences(
				"com.esprit.karima.turkish", Context.MODE_PRIVATE);
		Editor editor = params.edit();
		try {
			editor.putString("Favoris", ObjectSerializer.serialize(allFavoris));
		} catch (IOException e) {
			e.printStackTrace();
		}
		editor.commit();
		Log.i("items", allFavoris.toString());
	}

	public void SaveFirstTime() {
		SharedPreferences params = getSharedPreferences(
				"com.esprit.karima.turkish", Context.MODE_PRIVATE);
		Editor editor = params.edit();
		editor.putBoolean("firstTime", false);
		editor.commit();
		firstTime = false;
	}

	// M�thode qui sauvegarde la langue s�lectionn�e au t�l�phone
	public void setLanguageFromPhone(String l) {
		SharedPreferences params = getSharedPreferences(
				"com.esprit.karima.turkish", Context.MODE_PRIVATE);
		params.edit().putString("Language", l).commit();
		language = l;
	}

	// M�thode qui retourne la langue s�lectionn�e du t�l�phone
	public String getLanguageFromPhone() {
		SharedPreferences params = getSharedPreferences(
				"com.esprit.karima.turkish", Context.MODE_PRIVATE);
		language = params.getString("Language", "1");
		return language;
	}

	// Cette m�thode retourne la liste des s�ries par langue
	public ArrayList<Serie> getSeriesByLang() {
		ArrayList<Serie> temp = new ArrayList<Serie>();
		for (Serie s : allSeriesList) {
			if (s.getLangueseries().equals(language)) {
				temp.add(s);
			}
		}
		return temp;
	}

	// Cette m�thode retourne la liste des �pisodes par langue et par s�rie
	public ArrayList<Serie> getEpisodesBySerie(Serie s) {
		ArrayList<Serie> temp = new ArrayList<Serie>();
		for (Serie s1 : allEpisodesList) {
			if (s1.getLangueseries().equals(s.getID())) {
				temp.add(s1);
			}
		}
		return temp;
	}

	// Cette m�thode retourne la liste des �pisodes par langue et par s�rie
	public ArrayList<Serie> getEpisodesBySerieId(String id) {
		ArrayList<Serie> temp = new ArrayList<Serie>();
		for (Serie s1 : allEpisodesList) {
			if (s1.getLangueseries().equals(id)) {
				temp.add(s1);
			}
		}
		return temp;
	}
}
