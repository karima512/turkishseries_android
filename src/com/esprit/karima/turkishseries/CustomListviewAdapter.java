package com.esprit.karima.turkishseries;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esprit.karima.turkishseries.entities.Favoris;
import com.esprit.karima.turkishseries.entities.Serie;
import com.esprit.karima.turkishseries.utils.GlobalClass;
import com.esprit.karima.turkishseries.utils.ImageDownloader;
import com.esprit.karima.turkishseries.utils.LocalPath;

public class CustomListviewAdapter extends BaseAdapter {
	private FragmentActivity activity;
	private ArrayList<?> data;
	private static LayoutInflater inflater = null;
	private boolean isSeriesList;
	public Resources res;
	private GlobalClass global;
	private ArrayList<Favoris> favoris;
	Serie tempValues = null;
	int i = 0;

	public CustomListviewAdapter(FragmentActivity a, ArrayList<?> d,
			Resources resLocal, boolean isSeriesList) {
		global = (GlobalClass) a.getBaseContext().getApplicationContext();
		favoris = global.GetFavoris();
		activity = a;
		data = d;
		res = resLocal;
		this.isSeriesList = isSeriesList;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		if (data.size() <= 0)
			return 1;
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		public TextView text;
		public TextView text1;
		public TextView textWide;
		public ImageView image;
		public ImageButton stars;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder holder;

		if (convertView == null) {
			vi = inflater
					.inflate(
							global.getLanguageFromPhone().equals("2") ? R.layout.item_listview_serie
									: R.layout.item_listview_serie_arabic, null);
			holder = new ViewHolder();
			holder.text = (TextView) vi.findViewById(R.id.serie_title);
			holder.stars = (ImageButton) vi.findViewById(R.id.serie_rate);
			holder.image = (ImageView) vi.findViewById(R.id.serie_icon);
			vi.setTag(holder);
		} else
			holder = (ViewHolder) vi.getTag();
		if (data.size() <= 0) {
			holder.text.setText("No Data");
		} else {
			tempValues = null;
			tempValues = (Serie) data.get(position);
			new ImageDownloader(holder.image).execute(LocalPath.localPath
					+ "/files/" + tempValues.getImage());
			holder.text.setText(tempValues.getName());
			vi.setOnClickListener(new OnItemClickListener(position));
			holder.stars.setTag(Boolean.valueOf(false));
			holder.stars.setImageResource(R.drawable.star_big_off);
			for (Favoris fav : favoris) {
				if ((fav.isSerie() && isSeriesList)
						|| (!fav.isSerie() && !isSeriesList)) {
					if (fav.getID().equals(tempValues.getID())) {
						holder.stars.setTag(Boolean.valueOf(true));
						holder.stars
								.setImageResource(R.drawable.star_big_on);
					}
				}
			}
			holder.stars.setOnClickListener(new OnImageItemClickListener(
					position, holder.stars));
		}
		return vi;
	}

	private class OnImageItemClickListener implements OnClickListener {
		private int mPosition;
		private WeakReference<ImageButton> button;

		OnImageItemClickListener(int position, ImageButton btn) {
			mPosition = position;
			button = new WeakReference<ImageButton>(btn);
		}

		@Override
		public void onClick(View arg0) {
			Serie temp = (Serie) data.get(mPosition);
			ImageButton myButton = button.get();
			boolean myButtonTag = (Boolean) myButton.getTag();

			Favoris favori = null;
			if (isSeriesList)
				favori = new Favoris(true, temp.getID(), "", temp.getImage(),
						temp.getName());
			else
				favori = new Favoris(false, temp.getID(), temp.getLink(),
						temp.getImage(), temp.getName());
			boolean favoriFound = false;
			for (Favoris fav : favoris) {
				if (fav.equals(favori)) {
					favoriFound = true;
				}
			}
			if (!favoriFound && !myButtonTag) {
				Toast.makeText(activity, "Adding a favorite",
						Toast.LENGTH_SHORT).show();
				global.allFavoris.add(favori);
				global.SaveFavoris();
			}
			if (favoriFound && myButtonTag) {
				Toast.makeText(activity, "Removing a favorite",
						Toast.LENGTH_SHORT).show();
				global.allFavoris.remove(favori);
				global.SaveFavoris();
			}

			if (!myButtonTag) {
				myButton.setImageResource(R.drawable.star_big_on);
				myButton.setTag(Boolean.valueOf(true));
			} else {
				myButton.setImageResource(R.drawable.star_big_off);
				myButton.setTag(Boolean.valueOf(false));
			}
		}
	}

	private class OnItemClickListener implements OnClickListener {
		private int mPosition;

		OnItemClickListener(int position) {
			mPosition = position;
		}

		@Override
		public void onClick(View arg0) {
			if (isSeriesList) {
				global.seriesTemp = global.getEpisodesBySerie((Serie) data
						.get(mPosition));
				Fragment fragment = new EpisodesFragment();
				FragmentManager fragmentManager = activity
						.getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.addToBackStack(null).commit();
			} else {
				Intent i = new Intent(Intent.ACTION_VIEW,
						Uri.parse(((Serie) data.get(mPosition)).getLink()));
				activity.startActivity(i);
			}
		}
	}
}