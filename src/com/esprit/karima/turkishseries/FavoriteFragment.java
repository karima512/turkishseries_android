package com.esprit.karima.turkishseries;


import java.util.ArrayList;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.esprit.karima.turkishseries.entities.Favoris;
import com.esprit.karima.turkishseries.entities.FavorisItem;
import com.esprit.karima.turkishseries.utils.GlobalClass;
import com.esprit.karima.turkishseries.utils.Item;
import com.esprit.karima.turkishseries.utils.SectionItem;

public class FavoriteFragment extends Fragment implements Item{
	private ListView list;
	private FavoriteCustomListviewAdapter adapter;
	 ArrayList<Item> items = new ArrayList<Item>();
	 private GlobalClass global;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = (GlobalClass) getActivity().getBaseContext()
				.getApplicationContext();

		View rootView = inflater.inflate(R.layout.activity_main, container,
				false);

		rootView.setFocusableInTouchMode(true);
		rootView.requestFocus();
		rootView.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				Log.i("test", "keyCode: " + keyCode);
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					getActivity().getSupportFragmentManager().popBackStack(
							null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					return true;
				} else {
					return false;
				}
			}
		});
		Resources res = getResources();
		list = (ListView) rootView.findViewById(R.id.series_listview);
		ArrayList<Item> data = new ArrayList<Item>();
		ArrayList<Favoris> listeSeries = new ArrayList<Favoris>();
		ArrayList<Favoris> listeEpisodes = new ArrayList<Favoris>();
		for(Favoris favori : global.GetFavoris()){
			if(favori.isSerie()){
				listeSeries.add(favori);
			}else
				listeEpisodes.add(favori);
		}
		data.add(new SectionItem("Series"));
		for(Favoris fav : listeSeries){
			data.add(new FavorisItem(fav));
		}
		data.add(new SectionItem("Episodes"));
		for(Favoris fav : listeEpisodes){
			data.add(new FavorisItem(fav));
		}
		adapter = new FavoriteCustomListviewAdapter(getActivity(), data, res, false);
		list.setAdapter(adapter);
		return rootView;
	}
	
	
	@Override
	public boolean isSection() {
		// TODO Auto-generated method stub
		return false;
	}

}
